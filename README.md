# Cycometer A

DE: Dokumentation eines Messgeräts für den Fahrkomfort, bzw. die Fahrradwegqualität. Ein Beschleunigungssensor wird in einer Schleife abgefragt. Die drei Achsen werden zur Gesamtbeschleunigung zusammengefasst. Gemeinsam mit der GPS-Position wird 1x/Sekunde ein Datensatz mit Position und der gemessenen Höchstbeschleungigung abgelegt.

EN: Documentation of a measuring device for cycling comfort, respectively the quality of bicycle ways. A accelerometer is read in a loop. The overall accerleration is composited from the 3 axes. Togehter with GPS position the highest value is saved once per second.

![Bild](https://codeberg.org/MDradelt/Trackvisualisierung/raw/branch/master/img/20201026_FaFaFa_Beispieldatei_Magdeburg_Olvenstedter_Graseweg_und_Hundisburger_Straße.png)

![Bild](https://codeberg.org/MDradelt/Cycometer_A/raw/branch/main/img/Cycometer_A_Prototyp_mit_Lenker.jpg)

## Inhaltsverzeichnis/Table of Contents

* Teileliste/bill of materials
* Schaltplan/wiring diagram
* Software
  * Bibliotheken/libraries
* Bedienung/operation instructions
* Lizenz/licence

## Teileliste/bill of materials

|Pos.|Menge|Einheit|Benennung|Bemerkung|EN
|----|-----|-----|----------|----|---|
|  01|    1|Stück|NodeMCU V3|Microcontrollerboard mit ESP8266 <a href="https://codeberg.org/MDradelt/Cycometer_A/raw/branch/main/img/Cycometer_A_NodeMCU_V3.jpg"><img src="https://codeberg.org/MDradelt/Cycometer_A/raw/branch/main/img/Cycometer_A_NodeMCU_V3.jpg" width="48"></a>|controller|
|  02|    1|Stück|MPU6050   |3-Achs-Beschleunigungssensor inkl. Gyroskop <a href="https://codeberg.org/MDradelt/Cycometer_A/raw/branch/main/img/Cycometer_A_sensor_MPU6050.jpg"><img src="https://codeberg.org/MDradelt/Cycometer_A/raw/branch/main/img/Cycometer_A_sensor_MPU6050.jpg" width="48"></a>|acceleration sensor
|  03|    1|Stück|NEO-6M    |GPS-Sensor <a href="https://codeberg.org/MDradelt/Cycometer_A/raw/branch/main/img/Cycometer_A_sensor_GPS_Neo-6M.jpg"><img src="https://codeberg.org/MDradelt/Cycometer_A/raw/branch/main/img/Cycometer_A_sensor_GPS_Neo-6M.jpg" width="48"></a>|GPS sensor
|  04|    1|Stück|MicroSD-Kartenleser|<a href="https://codeberg.org/MDradelt/Cycometer_A/raw/commit/c0c7cc9c42e3c2753db783e534f3597d2626f880/img/Cycometer_A_MicroSD-Card%20Reader.jpg"><img src="https://codeberg.org/MDradelt/Cycometer_A/raw/commit/c0c7cc9c42e3c2753db783e534f3597d2626f880/img/Cycometer_A_MicroSD-Card%20Reader.jpg" width="48"></a>|card reader|
|  05|    1|Stück|MicroSD-Karte |FAT32-formatiert|MicroSD card|
|  06|    1|Stück|Schalter|ein/aus|switch on/off
|  07|    1|Stück|Widerstand R1|z.B. 22 kOhm (10-100 kOhm ist ausreichend)|resistor
|  08|    1|Stück|Gehäuse||case|
|  09|    1|Stück|Sensorgehäuse|inkl. Befestigung am Lenker|sensor case
|  10|    1|Stück|USB-Kabel|Programmierung und Stromversorgung|USB cable
|  11|    1|Stück|USB-Powerbank||USB power bank
|  12|   14|Stück|Kabel kurz|interne Verdrahtung|wire short: for internal wiring
|  13|    1|Stück|Kabel lang|4-adrig flexibel 1,5 m|4 wire cable: case to sensor
|  14|    1|Stück|Schrumpfschlauch für R1|alternativ Isolierband|heat shrink tubing/insulation material for R1|
|  15|    1|Stück|Schraubenset|Schlagworte: Hex Spacer; Abstandshalter|set of bolts, nuts and spacers
|  16|    2|Stück|Kabelverschraubung|für Sensorkabel M12x1,5|cable gland for sensor cable

### Hinweise zur Stückliste

*Pos. 05: Zum Zeitpunkt der Anleitungserstellung waren 4-GB-Karten kaum noch zu bekommen. 8 GB waren für unter 5 € erhältlich.

*Pos. 09 Vorschläge: Alte Fahrradstecklampe, Klingelgehäuse, Handyhalterung, etc. Die Einbaulage des Sensors ist frei wählbar.

*Pos. 11: Achtung: Einige Powerbanks schalten sich bei geringer Stromentnahme automatisch ab.

*Pos. 12: Kabel: Es können zum Teil Jumperkabel mit Steckverbindern aus dem Ardunio-Zubehör verwendet werden. Bei einigen dieser Kabel können die Stecker entfernt werden um sie direkt zu verlöten. Eine Packung Jumperkabel ist ausreichend für die Verdrahtung innerhalb des Gehäuses. Empfehlung für reine Lötbauweise: mehrfarbiges Flachbandkabel.

*Pos 13: Wir haben ein 4-adriges Telefonkabel verwendet und Steckverbinder aufgecrimpt. Auch USB-Kabel enthalten 4 nutzbare Adern. Alternative zum Crimpen: ungenutzte Stecker vom Jumperkabel (Pos. 12) anlöten.


## Schaltplan/wiring diagram

![Bild](https://codeberg.org/MDradelt/Cycometer_A/raw/commit/c7c93414159bfc2190820f859be6abdbd86d3a2e/img/Schaltplan_Cycometer_A_Rev01a.png)

## Software
DE: Die Software wurde mit der Arduino-Plattform erstellt. Die Plattform kann unter www.arduino.cc kostenlos heruntergeladen werden. Mit ihr wird der Programmcode auf den Mikroprozessor gespielt.

EN: The Arduino sofware platform was chosen for programming. Arduino platform software can be downloaded at www.arduino.cc without cost. It is used to bring the code on the microprocessor.

### Bibliotheken/libraries
DE: Folgende Arduino-Bibliotheken müssen für den Kompilierungsprozess zur Verfügung stehen:

EN: Following Arduino libraries must be available for compiling:

* tinyGPS++ Doku http://arduiniana.org/libraries/tinygpsplus/
* Softwarserial.h für GPS
* Wire.h: I2C für MPU6050
* SPI für Kartenleser
* SD.h für Kartenleser

Weiterhin ist unter Werkzeuge->Boardverwalter die Bibliothek:

*ESP8266 Boards 2.7.4

zu installieren. (Versionsnummer zum Zeitpunkt der Programmierung), Hinweise zur Installation: https://github.com/esp8266/Arduino

## Bedienung/operation instructions

|DE: Bedienung|EN: operation instructions|
|-------------|-------------|
|Der Sensor wird üblicherweise am Lenker montiert, um die dort auf den Körper einwirkenden Beschleunigungen zu erfassen. Die Einbaulage ist frei wählbar. Die Gesamtbeschleunigung wird aus allen drei Achsen zuammengefasst. Der Rest der Elektronik kann auch anderweitig untergebracht werden. Es hat sich als nützlich erwiesen, wenn der Schalter während der Fahrt erreichbar ist.|The sensor will be mounted usuallly at the steering bar to catch accerlerations with their impact on the body. The orientation is free. The overall acceleration composited from the three axes will be logged. The rest of electronics can be mounted or carried anywhere else. It has been proved helpful when the switch is in reach and can be operated while riding. 
|Das Einschalten erfolgt durch durch Herstellen der Spannungversorgung. Es wird durch die blinkende LED am Mikroprozessor signalisiert|Switching on is done by bringing Power to the main board. Its signaled by the blinking LED.
|Warten bis ein GPS-Fix vorliegt. Die LED am GPS-Sensor beginnt zu blinken.|Waiting until there is a GPS-Fix. The LED on the GPS-Sensor starts blinking.
|Das Aufzeichnen der Logdatei beginnt und endet durch Betätigen des Schalters.|Logging starts and stops by using the switch.
|Es wird eine Datei mit dem Datum aus den GPS-Daten pro Tag auf der Speicherkarte angelegt. Bei mehrmaligen Betätigen des Schalters wird jeweils eine Zwischenüberschrift eingefügt. Dies kann zur Abgrenzung verschiedener Tracks genutzt werden. Es wird ein Datensatz pro Sekunde gespeichert.| A File with Data from GPS as Name will be created on the memory card for every day. Interruptions by using the switch will create a new headline. This can be used to divide the file into tracks. One dataset per second will be saved.
|Vor dem Ausschalten sollte das Logging beendet werden um Datenfehler beim Trennen der Stromversorgung zu vermeiden.|Before powering off the logging should be stopped to prevent from data corruption while powering off.
|Das Ausschalten erfolgt durch Trennen der Stromversorgung.|Powering off is done by cutting the energy.
|Die Dateien auf der Speicherkarte können direkt mit den Vorgaben unter https://codeberg.org/MDradelt/Trackvisualisierung heraufgeladen und angezeigt werden.|The data files from the memory card can be direclly uploaded an visualized with the instructions from https://codeberg.org/MDradelt/Trackvisualisierung.

![Bild](https://codeberg.org/MDradelt/Cycometer_A/raw/branch/main/img/Cycometer_A_Prototyp_Detail_Anschl%c3%bcsse_Hauptgeh%c3%a4use.jpg)

## Lizenz/Licence

<p xmlns:cc="http://creativecommons.org/ns#" xmlns:dct="http://purl.org/dc/terms/"><span property="dct:title">Cycometer A</span> by <a rel="cc:attributionURL dct:creator" property="cc:attributionName" href="https://codeberg.org/MDradelt/Cycometer_A">MDradelt</a> is licensed under <a href="http://creativecommons.org/licenses/by-sa/4.0/?ref=chooser-v1" target="_blank" rel="license noopener noreferrer" style="display:inline-block;">CC BY-SA 4.0<img style="height:22px!important;margin-left:3px;vertical-align:text-bottom;" src="https://mirrors.creativecommons.org/presskit/icons/cc.svg?ref=chooser-v1"><img style="height:22px!important;margin-left:3px;vertical-align:text-bottom;" src="https://mirrors.creativecommons.org/presskit/icons/by.svg?ref=chooser-v1"><img style="height:22px!important;margin-left:3px;vertical-align:text-bottom;" src="https://mirrors.creativecommons.org/presskit/icons/sa.svg?ref=chooser-v1"></a></p> 